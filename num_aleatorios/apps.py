from django.apps import AppConfig


class NumAleatoriosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'num_aleatorios'
