from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.template import loader 
import random

class GeneradorAleatorios(View):
    def get(self,request):
        template = loader.get_template( 'aleatorios.html' )
        datos={
            "numeros":[]
        }
        for i in range(10):
            datos[ 'numeros'].append(random.randint( 0,10))
        pagina=template.render(datos)
        respuesta= HttpResponse(pagina)
        return respuesta
